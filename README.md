# pure-cheekbones
Egyptian Engineering Student. I ❤️ GNU/Linux, Dracula Theme and Satoshi Nakamoto.
<br /> <br />
<a href="https://discordapp.com/channels/@me/747449468864954438/">
  <img align="left" alt="t0va#7773" width="21px" src="https://raw.githubusercontent.com/anuraghazra/anuraghazra/master/assets/discord-round.svg" />
</a> pure-cheekbones#7773
<br />
<br />

<a href="https://github.com/anuraghazra/github-readme-stats">
  <img align="left" src="https://github-readme-stats.vercel.app/api?username=pure-cheekbones&show_icons=true&include_all_commits=true&theme=dracula" alt="Anurag's github stats" />
</a>

<br />
<br />

<a href="https://github.com/anuraghazra/github-readme-stats">
  <!-- Change the `github-readme-stats.anuraghazra1.vercel.app` to `github-readme-stats.vercel.app`  -->
  <img align="left" src="https://github-readme-stats.vercel.app/api/top-langs/?username=pure-cheekbones&layout=compact&theme=dracula" />
</a>
